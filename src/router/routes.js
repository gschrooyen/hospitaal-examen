
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '',
        component: () => import('pages/Index.vue')
      },
      { path: 'wards/:name',
        props: true,
        component: () => import('pages/Ward.vue')
      },
      { path: 'settings',
        component: () => import('pages/Settings.vue')
      },
      {
        path: 'wards/:name/patient/:id',
        props: true,
        component: () => import('pages/detail.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
