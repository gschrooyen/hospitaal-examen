import Axios from 'axios'

/*
export function someAction (context) {
}
*/
export function fetchWards ({ commit }, { self }) {
  Axios.get('http://localhost:3000/wards').then(function (result) {
    commit('FETCH_WARDS', result.data)
  }).catch((e) => {})
}

export async function updateSettings ({ commit }, { self }) {
  const settings = self.settings
  let prom = Axios.put('http://localhost:3000/Settings', settings)
  await self.$store.dispatch('hospitaal/fetchSettings')
  return prom
}

export function fetchSettings ({ commit }) {
  commit('FETCH_SETTINGS', Axios.get('http://localhost:3000/Settings'))
}

export async function getPatient ({ commit }, { self, patientId }) {
  let result = await Axios.get('http://localhost:3000/patients/' + patientId)
  return result.data
}

export async function getActions ({ commit }, { self, patientId }) {
  let result = await Axios.get('http://localhost:3000/actions/' + patientId)
  return result.data
}

export async function getWard ({ commit }, { self, wardName }) {
  let result = await Axios.get('http://localhost:3000/wards?name=' + wardName)
  return result.data
}

export function updatePatient ({ commit }, { patient }) {
  return Axios.put('http://localhost:3000/patients/' + patient.id, patient)
}

export function updateActions ({ commit }, { actions }) {
  return Axios.put('http://localhost:3000/actions/' + actions.id, actions)
}
