
/*
export function someGetter (state) {
}
*/
export function getWards (state) {
  return state.wards
}

export async function getSettings (state) {
  return state.settings
}

export function getInterval (state) {
  return state.intervalId
}
