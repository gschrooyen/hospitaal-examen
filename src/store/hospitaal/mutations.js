/*
export function someMutation (state) {
}
*/
export function FETCH_WARDS (state, wards) {
  state.wards = wards
}

export function FETCH_SETTINGS (state, settings) {
  state.settings = settings
}

export function SET_INTERVAL (state, intervalId) {
  state.intervalId = intervalId
}
